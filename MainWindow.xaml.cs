﻿using MahApps.Metro.Controls;
using PCStatus.ViewModel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PCStatus
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private bool closeRequested = false;
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel();
            Closing += MainWindow_Closing;
        }

        void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!closeRequested)
            {
                e.Cancel = true;
                NotifyIcon ni = new NotifyIcon();
                ni.Icon = PCStatus.Properties.Resources.MineCraft;
                ni.Visible = true;
                ni.DoubleClick += ni_DoubleClick;
                ni.MouseClick += ni_MouseClick;
                this.Hide();
            }

        }

        void ni_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {

            ContextMenuStrip strip = new ContextMenuStrip();
            strip.Items.Add("Exit");
            strip.ItemClicked += (s, a) =>
            {
                if (a.ClickedItem.Text.Equals("Exit"))
                {
                    closeRequested = true;
                    this.Close();
                }

            };
            strip.Show(System.Windows.Forms.Cursor.Position);
        }

        void ni_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = WindowState.Normal;
        }

    }
}
