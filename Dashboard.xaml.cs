﻿using MahApps.Metro.Controls;
using PCStatus.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PCStatus
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : MetroWindow
    {
        public Dashboard()
        {
            InitializeComponent();
            DataContext = new DashboardViewModel();
            //Style s = new Style();
            //s.Setters.Add(new Setter(UIElement.VisibilityProperty, Visibility.Collapsed));
            //tabControl.ItemContainerStyle = s;
        }

     
    }
}
