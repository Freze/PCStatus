﻿using ComputerLib;
using ComputerLib.Hardware;
using ComputerLib.SystemC;
using Microsoft.Win32;
using PCStatus.Model;
using PCStatus.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace PCStatus.ViewModel
{
    public class DashboardViewModel : INotifyPropertyChanged
    {
        //Very IMPORTANT - Add an emergency combination in case something goes wrong and the pc remain locked.
        private Computer computer;

        public DashboardViewModel()
        {
            computer = new Computer();
            switch (computer.GPU.SubSystem)
            {
                case Manufacturer.MSI:
                    BGPath = "Backgrounds/msi.png";
                    break;
                default:
                    BGPath = "Backgrounds/intel.png";
                    break;
            }
            Processes p = new Processes();
            Processes = p.ToCollection();
        }


        #region Processes
        private ObservableCollection<ProcessHolder> processes;
        public ObservableCollection<ProcessHolder> Processes
        {
            get { return processes; }
            set
            {
                processes = value;
                OnPropertyChanged("Processes");
            }
        }
        #endregion

        #region OpenProccess

        private ICommand openProcess;
        public ICommand OpenProccess
        {
            get
            {
                if (openProcess == null)
                    openProcess = new RelayCommand(true, (o) => { CurrentTab = 1; });
                return openProcess;
            }
        }

        public void OpenProccessAction()
        {

        }

        #endregion

        #region AddGame & Games

        private ObservableCollection<Game> games = new ObservableCollection<Game>();
        public ObservableCollection<Game> Games
        {
            get { return games; }
        }


        private ICommand addGame;
        public ICommand AddGame
        {
            get
            {
                if (addGame == null)
                    addGame = new RelayCommand(true, (o) =>
                    {
                        OpenFileDialog dialog = new OpenFileDialog();
                        dialog.Multiselect = false;
                        dialog.ShowDialog();
                        games.Add(new Game(dialog.SafeFileName, dialog.FileName));
                    });
                return addGame;
            }
        }

        #endregion

        #region LaunchGame

        private ICommand launchGame;
        public ICommand LaunchGame
        {
            get
            {
                if (launchGame != null)
                    launchGame = new RelayCommand(true, LaunchTheGame);
                return launchGame;
            }
        }

        private void LaunchTheGame(object obj)
        {
            
        }

        #endregion

        #region CurrentTab

        private int currentTab;
        public int CurrentTab
        {
            get { return currentTab; }
            set
            {
                currentTab = value;
                OnPropertyChanged("CurrentTab");
            }
        }

        #endregion

        #region OpenMenu And IsMenuOpen

        private ICommand openMenu;
        public ICommand OpenMenu
        {
            get
            {
                if (openMenu == null)
                    openMenu = new RelayCommand(true, OpenMenuAction);
                return openMenu;
            }
        }

        public void OpenMenuAction(object obj)
        {
            IsMenuOpen = true;
        }

        private bool isMenuOpen;
        public bool IsMenuOpen
        {
            get { return isMenuOpen; }
            set
            {
                isMenuOpen = value;
                OnPropertyChanged("IsMenuOpen");
            }
        }

        #endregion

        #region BGPath
        private string bgpath;
        public String BGPath
        {
            get { return bgpath; }
            set
            {
                bgpath = value;
                OnPropertyChanged("BGPath");
            }

        }
        #endregion

        #region Notify

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string op)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(op));
        }

        #endregion
    }
}
