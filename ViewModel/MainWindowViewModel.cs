﻿using PCStatus.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace PCStatus.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {

        public MainWindowViewModel()
        {

        }

        #region BorderBrush

        private Brush brush = Brushes.White;

        public Brush BorderBrush
        {
            get { return brush; }
            set
            {
                brush = value;
                OnPropertyChanged("BorderBrush");
            }
        }

        public void DoError()
        {
            BorderBrush = Brushes.Red;
        }

        #endregion

        #region AgreeButton
        private string agreeButtonText;
        private bool agreeButtonEnable;

        public string AgreeButtonText
        {
            get { return agreeButtonText; }
            set
            {
                agreeButtonText = value;
                OnPropertyChanged("AgreeButtonText");
            }
        }
        public bool AgreeButtonEnabled
        {
            get { return agreeButtonEnable; }
            set
            {
                agreeButtonEnable = value;
                OnPropertyChanged("AgreeButtonEnabled");
            }
        }

        public void StartCounting()
        {
            AgreeButtonEnabled = false;
            for (int i = 4; i > 0; i--)
            {
                AgreeButtonText = "I Agree..." + i;
                Thread.Sleep(1000);
            }
            AgreeButtonText = "I Agree";
            AgreeButtonEnabled = true;
        }

        private ICommand agreeCommand;
        public ICommand AgreeCommand
        {
            get
            {
                if (agreeCommand == null)
                    agreeCommand = new CommandHandler(true, LaunchDashboard);
                return agreeCommand;
            }
        }
        public void LaunchDashboard()
        {
            new Dashboard().Show();
        }


        #endregion

        #region Login

        private ICommand loginCommand;

        public ICommand LoginCommand
        {
            get
            {
                if (loginCommand == null)
                    loginCommand = new CommandHandler(true, LoginHandler);
                return loginCommand;
            }
        }

        public void LoginHandler()
        {
            try
            {
                Firebase.FirebaseInstance.LoginWithPassword(username, password);
                CurrentTabIndex = 1;
                Task.Factory.StartNew(StartCounting);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Username or email are wrong!");
                Password = "";
                Username = "";
                DoError();
            }


        }


        private string username, password;
        public string Username
        {
            get { return username; }
            set
            {
                username = value;
                OnPropertyChanged("Username");
            }
        }
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                OnPropertyChanged("Password");
            }
        }

        #endregion

        #region CurrentTabIndex

        private int currentIndex = 0;

        public int CurrentTabIndex
        {
            get { return currentIndex; }
            set
            {
                currentIndex = value;
                OnPropertyChanged("CurrentTabIndex");
            }
        }

        #endregion

        #region Notify

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string op)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(op));
        }

        #endregion
    }
}
