﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PCStatus.Model
{
    public class Game : INotifyPropertyChanged
    {
        private string title, path;
        private ImageSource icon;

        public Game(string title, string path)
        {
            Title = title;
            Path = path;
            System.Drawing.Icon icon = System.Drawing.Icon.ExtractAssociatedIcon(path);
            Icon = Imaging.CreateBitmapSourceFromHBitmap(
          icon.ToBitmap().GetHbitmap(), IntPtr.Zero, Int32Rect.Empty,
          BitmapSizeOptions.FromEmptyOptions());
        }

        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                OnPropertyChanged("Title");
            }
        }

        public string Path
        {
            get { return path; }
            set
            {
                path = value;
                OnPropertyChanged("Path");
            }
        }

        public ImageSource Icon
        {
            get { return icon; }
            set
            {
                icon = value;
                OnPropertyChanged("Icon");
            }
        }

        #region Notify

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string op)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(op));
        }

        #endregion
    }
}
